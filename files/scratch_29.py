from matplotlib import pyplot as plt
from matplotlib import style
import numpy as np

import pandas as pd
import scipy

from scipy import signal

style.use('fivethirtyeight')

t,e,l = np.loadtxt('Jifei13.csv', unpack = True, delimiter = ',')
l = abs(l)
lo = signal.savgol_filter(l, 3, 1)
e = abs(e)
plt.plot(e,l, color = 'deepskyblue', linewidth = 2, label = 'Raw Data')
plt.plot(e,lo, color = 'fuchsia', linewidth = 2, label = 'Savitzky-Golay filtered data')
plt.legend(loc= 'upper left')
plt.xlabel('displacement (mm)')
plt.ylabel('Tension load (N)')
plt.title('OTC Industries #13. Tension Test')
plt.show()
