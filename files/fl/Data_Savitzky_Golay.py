from matplotlib import pyplot as plt
from matplotlib import style
import numpy as np
from scipy import signal

#/////////////////////////////////////  openig file, reading and filtering time dependandt dataset. ///////////////////////////////////////


names =['p4t1','p4t2','p4t3','p4b1','p4b2','p4b3','p5t1','p5t2','p5t3','p5b1','p5b2','p5b3','p6t1','p6t2','p6t3','p6b1','p6b2','p6b3','p7t1','p7t2','p7t3','p7b1','p7b2', 'p7b3','p8t1','p8t2','p8t3','p8b1','p8b2','p8b3']

#sample_name = 'p1t1' #introduce  here the name of the csv file without the csv extension. It also will use this name to generate the png image data


for name in range(len(names)):


    t,e,l = np.loadtxt(names[name] + '.csv', unpack = True, delimiter = ',') #reading rows of the csv. First column is time, second is extension and the last one is force
    l = abs(l) # making all positive. So we can define later compresion test or tension test but newtons will be always positive values.
    lo = signal.savgol_filter(l,5, 1) #Savitzky-Golay filter. Firts argument is dataset, secon argument is k+1 frame length (allways odd). Those are the polinomial regresion points used to estimate the center data.
                                        # Its odd because the center value must be symmetric

    e = abs(e)

    #////////////////////////////////////////  plotting stuff   //////////////////////////////////////////////////////

    style.use('fivethirtyeight') #style used
    plt.plot(e,l, color = 'deepskyblue', linewidth = 2, label = 'Raw Data')
    plt.plot(e,lo, color = 'fuchsia', linewidth = 2, label = 'Savitzky-Golay filtered data')
    plt.legend(loc= 'upper left')
    plt.xlabel('y component deflection (mm)')
    plt.ylabel('load (N)')
    plt.title('Tracking Number '+ names[name])
    plt.savefig( names[name] + '.png', bbox_inches='tight')

    plt.show()

