# COVID-19 Swab Testing

### Page Menu
* Click here for [allowables](https://gitlab.cba.mit.edu/alfonso/swab-testing-for-covid-19/-/blob/master/README.md#allowable-values-testing-official-np-swabs-at-cba-instron)
* Click here for [OPT swab](https://gitlab.cba.mit.edu/alfonso/swab-testing-for-covid-19/-/blob/master/README.md#first-candidate-opt-industries-swab-by-jifei)
* Click here for [resin printed swab](https://gitlab.cba.mit.edu/alfonso/swab-testing-for-covid-19/-/blob/master/README.md#second-candidate-resin-printed-swabs)


# Introduction

In this page all design proposed as swabs are going to be tested. Thanks to Martin Culpepper we could receive 4 NP Swabs and those are being tested to serve as a reference for future models done.

The tests to determine the viability of any design will be:

  - **Bending Test**

  Consisting on bending the swab 35 degrees while measuring the vertical component of the reaction force.


  - **Tip Compression Test**

  Compression test to determine the resistant force to be compressed of the hairy tip of the swabs when compressed until the nominal value of the hard stick that holds it. It is wanted to compress only the tip, not the core.


  - **Final Tension Test**

  Destructive tension test that sets the failure point when submitted to a tension force.

  - **Axial Torsion Fatigue Test**

  Static load test that determines the value of the angle in which a constant tension start decreasing when rotating the swab.


**IMPORTANT!** Two more test could be designed. Those one will be related with the amount of particles collected in the tip.


# **Allowable values**. Testing official NP Swabs at CBA Instron

<img src="images/OffNPS.jpeg" alt="plot1" width="900"/>

In this section, results of all experiments will be posted and can be used to compare with any value obtained by any proposed design. With difficulty, we have been able to receive 4 samples. That's why it was not possible to make multiple times some tests to establish a mean value and a deviation.

## **Bending Test**

Tooling was designed to this purpose.
<img src="images/mount.jpeg" alt="plot1" width="1000"/>


![gif](images/test.gif)

The movable part of the Instron will compress the swab in a cantilever, making it to bend until the upper part of the tooling touches the lower part of the tooling. We will be able to see in the data when the step in the force happens, being able to see the previous instant which will correspond with the force tha the swab makes under a 35 degrees bending.

The official NP Swab mounted in the tooling looks like the following:

![NP Bending](images/NPBending.jpeg)

Results obtained for this test are:

<img src="images/npbend.png" alt="plot1" width="500"/>


Two clear region can be seen. The maximum load that this first linear slope reaches is 0.18N. Up to that it start growing linear but with a much higher slope until 1.2N value is reached.


## **Tip Compression Test**

This test consist on compressing the hairy tip of the swab. As we don't want to compress the main stick that holds the tip, the value of the thickness will be our criteria to stop the test.

The lay-up shows as follows here:
<img src="images/compressedNP.jpeg" alt="plot1" width="900"/>



The results are the following:

<img src="images/npcomptest.png" alt="plot1" width="500"/>



Exponential grow can be seen reaching a maximum value of 2.5N when reached the nominal thickness of the stick. Similar behavior has been seen with cotton swabs in this same test.


## **Final Tension Test**

An NP Swab has been submitted to an ultimate tensile stiffness until failure. The geometry of the NP swab and the material will determine the behavior of this test. As it is monolithic and solid we will be able to see a classic elastic deformation reaching to a plastic deformation. The value of this second slope will be determined by the material.

The lay-out of this is the following:

<img src="images/NPTension.jpeg" alt="plot1" width="900"/>


And results are the following

<img src="images/nptensiontest.png" alt="plot1" width="500"/>


The swab has been able to deform 16mm. The marks of the clamping system were statics so the test is valid. Also, the deformation compared with another partner (after the natural shrink after breaking) is huge!

![comp](images/NPelongated.jpeg)


## **Axial Torsion Fatigue Test**

This test has been tricky due to our machine is only available to make linear displacements. I have designed and manufactured tooling to be used in tension but able to have a degree of freedom, the rotation along this same axis.

<img src="images/NPTension1.jpeg" alt="plot1" width="900"/>


The test is described as following: I have pre-tensioned the swabs into a 8N force. I have started a tension test in the instron with a feed of 0.1mm/min (almost nothing). I have been rotating and counting the tooling until the tension force dropped suddenly in the test. when this happens, we have broke internally the swab by this fatigue torsion caused.

Tooling designed for the test is here:

![tooling](images/GigsToolrotate.jpeg)

![assyrot](images/assyrot.jpeg)

![rotatingGig](images/rotatingGiG.gif)


![gif](images/axialtest.gif)

The result for this test is, the tension has dropped up to 2N suddenly when I have passed a rotation of **710º**.

|                   | **NP Swab** |
|:-----------------:|:-----------:|
| **Angle Reached** |     710º    |





# **First Candidate**. OPT Industries Swab by Jifei.

Jifei provide us 16 models to be tested.

![samples](images/samples.jpeg)
![samples2](images/samples2.jpeg)
![samples](images/jifeisamples.jpeg)

I have developed to them all tests described bellow. Here you can find the results.

### **Bending Test**

Results for all elements are the following. Raw data can get download on the files folder.

#### Sample 1

<img src="images/plot1.png" alt="plot1" width="500"/>


#### Sample 2
<img src="images/plot2.png" alt="plot1" width="500"/>

#### Sample 3
<img src="images/plot3.png" alt="plot1" width="500"/>

#### Sample 4
<img src="images/plot4.png" alt="plot1" width="500"/>

#### Sample 5
<img src="images/plot5.png" alt="plot1" width="500"/>


#### Sample 6
<img src="images/plot6.png" alt="plot1" width="500"/>


#### Sample 7
<img src="images/plot7.png" alt="plot1" width="500"/>

#### Sample 8
<img src="images/plot8.png" alt="plot1" width="500"/>

#### Sample 9
<img src="images/plot9.png" alt="plot1" width="500"/>

#### Sample 10
<img src="images/plot10.png" alt="plot1" width="500"/>


A plot comparison of all results vs. NP swab will be done. For now, this table will serve us as a first comparison.

|                               | **NP Swab** | **OCT Industries** |
|:-----------------------------:|:-----------:|--------------------|
| resultant force at 35º (mean) |     1.2N    |        0.27N       |

Despite the difference in the ultimate value, the initial slope of bending is so similar and well worth to compare

|                                | **NP Swab** | **OCT Industries** |
|:------------------------------:|:-----------:|--------------------|
| resultant force at 16mm (mean) |    0.18N    |        0.113N        |




After watch 10 test of this elements, it can be seen that there is a linear part of the test that represent the stiffness of the beam but suddenly, in all samples, the slope changes and it represent the compression of the tip of the swab.

The elements 6,7,8,9, and 10 (different curing parameters) can be seen that the resin cycle has affect slightly to an increment of stiffness. From a 0.125 N mean value of the 5 first elements, the next 5 elements shows a mean value before the commented slope of 0,164 N.


### **Tip Compression Test**


Swab tips were provided as well as cotton tips to compare this same test as described bellow. Jifei design has void tips, making the deformation more linear than Official NP swabs and cotton swabs.  

The samples provided are:

![tipSwabs](images/comp.jpeg)


The two OCT samples were tested as well as the 4 cotton tips of the cot1 and cot2 elements.

The lay up of this test is the following:

![test2](images/test2.gif)

Results for the sample done by OCT Industries are:

<img src="images/oct1.png" alt="plot1" width="500"/>

<img src="images/oct2.png" alt="plot1" width="500"/>


Can be seen that the compression is very linear and in both elements, under a compresion of 1.3mm the value is 8N.

For the cotton tips, results are interesting.
<img src="images/oct1.png" alt="plot1" width="500"/>

<img src="images/cot2.png" alt="plot1" width="500"/>

<img src="images/cot3.png" alt="plot1" width="500"/>

<img src="images/cot4.png" alt="plot1" width="500"/>


As cotton cant ensure a fixed geometry, the results has a biggest deviation. Also, as we are accumulating mass when compressing, the grow of the force seems more likely as an exponential. OCT design doesn't behave the same due to its internally hollow.


Files and python script can be found inside the project folder.

Comparison with official results are here:

|                               | **NP Swab** | **OCT Industries** | **Cotton** |
|:-----------------------------:|:-----------:|--------------------|------------|
| resultant force at ult (mean) |      7N     |        9.1N        | 11.2N      |


### **Tension Test**

Swabs number 11 12 and 13 were mounted in the tensile clamps of the instron to develop the test.

![tension](images/JiffeiTension.jpeg)

Results are the following:
<img src="images/tension11.png" alt="plot1" width="500"/>
<img src="images/tension12.png" alt="plot1" width="500"/>
<img src="images/tension13.png" alt="plot1" width="500"/>




Clearly this element is elastic enough to break without reaching a clear plastic deformation. Values can be compared in the following table:

|                          | **NP Swab** | **OCT Industries** |
|:------------------------:|:-----------:|:------------------:|
| Tension value at failure |     34N     |         20N        |
|     Max displacement     |     17mm    |         4mm        |

After analyze the results, clearly the number 11 shows what has happened in the tension test. The first time I clamped the swabs without a rubber, damaging locally more the tip of the number 11, breaking more locally along the clamp edge and reaching a lower performance than the rest as can be seen in data.




## **Axial Torsion Fatigue Test**

Samples number 14, 15 and 16 were tested as described.  

![NPTension](images/NPTension1.jpeg)

Results are:

| **OCT sample** | **Angle Reached** |
|:--------------:|:-----------------:|
|       14       |        910º       |
|       15       |        660º       |
|       16       |        940º       |


Compared with official NP Swab:

|                   | **NP Swab** | **OCT Industries** |
|:-----------------:|:-----------:|:------------------:|
| **Angle Reached** |     710º    |        837º        |

# **Second Candidate**. Resin printed Swabs.

***Raw data in csv format and python script used to read, filter and plot information can be found in the repo folders***

A couple days ago some lab friends reached us to test mechanically some of their proposed designs. On April 25th three different swabs were given. It is going to be used some tracking names to track the model and the test done as the chart shows:

<img src="images/fl/all.jpeg" alt="samples" width="600"/>

|                  | **Model/pn 1** | **Model/pn 2** | **Model/pn 3** |
|:----------------:|:-----------:|:-----------:|:-----------:|
| **Bending test** |     p1b     |     p2b     |     p3b     |
| **Tension test** |     p1t     |     p2t     |     p3t     |
|  **Axial test**  |     p1a     |     p2a     |     p3a     |

Also, each experiment will be done on two or three swabs. An additional name will be added indicating the number of the experiment.

### **Bending Test**

<img src="images/fl/bending.jpeg" alt="samples" width="600"/>


Data for the swab with PN 1, PN 2 and PN 3 is the following. It can be seen that the resin used is the most rigid of all the swabs it has been tested here. Also, the diameter of each section is bigger than swab pn2 and swab pn3.


![charts](files/fl/p1b1.png)
![charts](files/fl/p1b2.png)

PN2 has got a similar external  shape as PN1 but the resin seems more malleable in terms of bending. Even though the results doesn't show this behavior that clearly as in real life, this could be caused because when we bend with our hands the swab we clearly go over the 35 degrees that this test do. And the difference could be in the ultimate bending angle, not in the strength needed to bend 35 degrees.

![charts](files/fl/p2b1.png)
![charts](files/fl/p2b2.png)


PN3 has got clearly the thinest cross section and is the easier to bend as results shown. Almost one order of magnitude from its partners. The huge peak shown in the swab with pn3 correspond with the compression of the tip so those values should be discarted.

![charts](files/fl/p3b1.png)
![charts](files/fl/p3b2.png)
![charts](files/fl/p3b3.png)

The experiment used as usually the specific Tooling

![charts](images/fl/p2b.jpeg)

Another interesting point is that any of the swabs shown any spring back deformation after the bending test.


### **Tension Test**

The ultimate tension test shows the maturity of the process. Value are so constant for each member of the family. All the swabs shown the capacity to resist almost 3 times more load than the official NP swab. Also, the type of breakage was so fragile and immediate, with a clear defined fragile plane.

PN1 showed a maximun peak so constant in all samples of almos t 140N when deflecting 3-4mm.

![charts](files/fl/p1t1.png)
![charts](files/fl/p1t2.png)
![charts](files/fl/p1t3.png)

The breakage point in all samples were in the transition radius from the tip to the main beam.


PN2 samples , as we commented before, seems a more elastic resin that also holds a bigger plastic deformation part of the chart as it can be seen. The deformation is almost the double, than PN1 and holding 100N. Surprisingly, the breaking point happened in the transition from the big beam section to the reduced beam section in all of the samples.

![charts](images/fl/p2t.jpeg)


![charts](files/fl/p2t1.png)
![charts](files/fl/p2t2.png)
![charts](files/fl/p2t3.png)

The biggest surprise came with the PN3. The comparation with the other swabs make it seems the most brittle with a huge difference. But surprisingly, with almost the half of the mass than its partners, PN3 holds a mean of 88N, 2.3 more times than an official NP swab. Also with a fragile way of breaking, in the middle of the thin section
![charts](images/fl/p3t.jpeg)

Chamfers in all the transition locates the breakage point in a much more secure place. It is needed to avoid an uncontrolled breakage close to the tip inside a nose, so this is the safest design with difference.

![charts](files/fl/p3t1.png)
![charts](files/fl/p3t2.png)
![charts](files/fl/p3t3.png)




### **Axial Torsion Fatigue Test**

![charts](images/fl/p3a3.jpeg)



The last test is the Axial Torsion Fatigue Test. Swabs were loaded up to 10N and turned into the value decreases close to the half.

My personal expectation was that this swab, because of the type of resin were much more brittle. But results shows that absolutely no. PN3 has the record with 7 turns before breaking with is impressive. It is so remarkable that also, the breaking was so so fragile and explosive. Any other swab in this test broke so plastically but this 3 models behaves so fragile as well in this test.

PN1 behaved incredibly flexible, able to make as a mean 3 turns

| **PN** | **Angle Reached** |
|:--------------:|:-----------------:|
|       p1a1       |        950º       |
|       p1a2       |        1100º       |
|       p1a3       |        1130º       |



PN2 started to shown its more flexible behavior. Up to 4 turns, the vale of the tension did not reduced to the half. The brittle breakage happened as mean in the 7th turn.

| **PN** | **Angle Reached until reduce tension** | **Angle of breaking** |
|:--------------:|:-----------------:|:-----------------:|
|       p2a1       |        1420º       |  2600º       |
|       p2a2       |        1460º    |2450º       |
|       p2a3       |        1590ªº       |2580º      |

PN3 behaved similarly to PN2, having as a mean 3 turns until reduce the tension and breaking at the 7th lap as well.

| **PN** | **Angle Reached until reduce tension** | **Angle of breaking** |
|:--------------:|:-----------------:|:-----------------:|
|       p3a1       |        940º       |  2300º       |
|       p3a2       |        1230º    |2650º       |
|       p3a3       |        1020ªº       |2370º      |

![charts](images/fl/p3a.jpeg)

## Second round. Testing Part Numbers p4,p5,p6,p7 and p8.
![charts](images/fl/samples2.jpeg)
To all of them tension test, bending test and torsion test were done. Swab7 was the first one delivered by this team with a non-solid tip. It can be seen in the tension test that the breaking point of all the p7samples are in the tip, breaking more elastically than the rest of the samples.

## Tension test
p4t1 meaning: p4 refers to the Part Number, T refers to Tension and 1 refers to the number of experiment.
![charts](files/fl/p4t1.png)
![charts](files/fl/p4t2.png)
![charts](files/fl/p4t3.png)
![charts](files/fl/p5t1.png)
![charts](files/fl/p5t2.png)
![charts](files/fl/p5t3.png)
![charts](files/fl/p6t1.png)
![charts](files/fl/p6t2.png)
![charts](files/fl/p6t3.png)
![charts](files/fl/p7t1.png)
![charts](files/fl/p7t2.png)
![charts](files/fl/p7t3.png)
![charts](files/fl/p8t1.png)
![charts](files/fl/p8t2.png)
![charts](files/fl/p8t3.png)

As a detail, a common failure mode of the PN7 when manipulated (without making a remarkable force on the tip) was this:
![charts](images/fl/pn7break.jpeg)


This image shown all the breaking points of the samples tested

![charts](images/fl/breakingmode2.jpeg)

## Bending Test

Same test as the first round.
p4b1 meaning: p4 refers to PN4, B means Bending Test, 1 means the number of sample.
![charts](files/fl/p4b1.png)
![charts](files/fl/p4b2.png)
![charts](files/fl/p4b3.png)
![charts](files/fl/p5b1.png)
![charts](files/fl/p5b2.png)
![charts](files/fl/p5b3.png)
![charts](files/fl/p6b1.png)
![charts](files/fl/p6b2.png)
![charts](files/fl/p6b3.png)
![charts](files/fl/p7b1.png)
![charts](files/fl/p7b2.png)
![charts](files/fl/p7b3.png)
![charts](files/fl/p8b1.png)
![charts](files/fl/p8b2.png)
![charts](files/fl/p8b3.png)

## Axial Fatigue Testing

p4a1 meaning: p4 is PN4, a means Axial Fatigue Test, 1 refers to the # of sample

| **PN** | **Angle Reached until reduce tension** | **Angle of breaking** |
|:--------------:|:-----------------:|:-----------------:|
|       p4a1       |        700º       |  1020º       |
|       p4a2       |        840º    |1200º       |
|       p4a3       |        720º       |990º      |
|       p5a1       |        1000º       |1560º      |
|       p5a2       |        1590º       |1330º      |
|       p5a3       |        1500º       |1600º      |
|       p6a1       |        1000º       |1340º      |
|       p6a2       |        1100º       |1300º      |
|       p6a3       |        940º       |1270º      |
|       p7a1       |        1400º       |1800º      |
|       p7a2       |        1550º       |2020º      |
|       p7a3       |        1630º       |2040º      |
|       p8a1       |        850º       |1250º      |
|       p8a2       |        770º       |1200º      |
|       p8a3       |        800º       |1180º      |
